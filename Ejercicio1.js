const {Sequelize} = require("sequelize");

const sequelize = new Sequelize(
    'BDsemana4',
    'javier',
    'jazmin303003',
    {
        host:'localhost',
        dialect:'mssql'

});


sequelize.authenticate()
.then( () =>{
    console.log('Conexión establecida satisfactoriamente');
})
.catch(()=>{
    console.log('Conexión no establecida');
});


class Users extends Sequelize.Model{}

// CREACIÓN DE LA TABLA

Users.init({
    ID:{ type:Sequelize.INTEGER, primaryKey:true,autoIncrement: true },
    Nombre: { type: Sequelize.STRING },
    Apellido: { type: Sequelize.STRING }
},{sequelize, modelName: 'users'});



// INSERCIÓN DE UN REGISTRO

sequelize.sync()
.then(()=> Users.create({
    Nombre: 'Mario',
    Apellido:'Novo'
})).then( f => {
    console.log(f.toJSON());
});


// ACTUALIZACIÓN DEL REGISTRO
/*
Users.update({
    Nombre :'Esteban'
},
{
    where:{id:'1'}
})
*/