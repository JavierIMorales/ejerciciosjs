const Sequelize = require('sequelize');

const sequelize = new Sequelize('BDsemana4', 'javier', 'jazmin303003',{
    host: 'localhost',
    dialect: 'mssql',
});

sequelize.authenticate()
.then(()=>{
    console.log("Exito en la conexión..!!");
})
.catch(error => {
    console.log('Fracaso la conexión', error)
});

class User extends Sequelize.Model{}

User.init({
    ID:{ type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    Nombre:{ type: Sequelize.STRING },
    Apellido: { type: Sequelize.STRING }
},{
    sequelize, modelName:'users'
});

// INSERCIÓN DE UN REGISTRO
sequelize.sync()
.then( ()=> User.create({
    Nombre: 'Marcos',
    Apellido: 'Rojo'
}))


// ELIMINACIÓN DE UN REGISTRO
/*
User.destroy({
    where: { ID: 1}
});
*/


