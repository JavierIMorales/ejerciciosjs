const Sequelize = require('sequelize');

const sequelize = new Sequelize('BDsemana4', 'javier', 'jazmin303003',{
    host: 'localhost',
    dialect: 'mssql'
});

sequelize.authenticate()
.then(()=>{
    console.log('Conexión exitosa con la base de datos.')
})
.catch(err => {
    console.log('Conexión fallida con la base de datos.', err)
});

const Model = Sequelize.Model

class Usuario extends Model{}
Usuario.init({
    ID:{type: Sequelize.INTEGER, primaryKey:true, autoIncrement: true},
    Nombre:{type: Sequelize.STRING},
    Apellido:{type: Sequelize.STRING}
},{sequelize, modelName: 'users'});


//CREACIÓN DE VARIOS REGISTROS
Usuario.create({
    Nombre: 'Roberto',
    Apellido: 'Petinatto'
});

Usuario.create({
    Nombre: 'Patricia',
    Apellido: 'Sosa'
});

Usuario.create({
    Nombre:'Mario',
    Apellido: 'Pergollini'
});


// ACTUALIZACIÓN DE VARIOS REGISTROS
/*
Usuario.update(
    {Nombre: 'Antonio'},{where:{ID : '3'}},
);
Usuario.update(
    {Nombre: 'Gillermo'},{where:{ID : '4'}},
);
Usuario.update(
    {Nombre: 'Laura'},{where:{ID : '5'}},
);
*/
